metadata:
  name: apertis-update-manager-rollback
  format: "Apertis Test Definition 1.0"
  image-types:
    minimal: [ armhf ]
  image-deployment:
    - OSTree
  type: functional
  exec-type: manual
  priority: low
  maintainer: "Apertis Project"
  description: "Test the automatic rollback and blacklist mechanism of apertis-update-manager.
                The automated version of this test: https://qa.apertis.org/aum-rollback-blacklist.html"

  resources:
    - "A static update bundle file of the same architecture, variant and version as the testing image"
    - "A Fat32 USB flash drive, preloaded with the update bundle at the root of the disk"
    - "The latest static update file can be downloaded at the same location than the target image. It has the same basename, and a '.delta' extension"
    - "The static update file should be copied to the flash drive using the name 'static-update.bundle'."
    - "A PC must be connected to DUT serial port"

  expected:
    - "U-Boot is able to detect rollback situation"
    - "U-Boot is able to use rollback configuration for bootloader"
    - 'The "failed" update is rolled back'
    - '"Failed" update is marked as blacklisted'
    - 'Apertis-update-manager is able to detect blacklisted update and refuse to update the system with it' 

run:
  steps:
    - "Check the initial deployment"
    - $ sudo ostree admin status
    - "Prepare the copy of commit and deploy to allow the upgrade to the same version"
    - "Command below shows you an initial commit ID, for instance"
    - |
        $ export BOOTID=$(sudo ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p'); echo $BOOTID
    - "Get the Collection ID and ref"
    -  $ export CID=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 1 -d ' '); echo COLLECTION_ID=$CID
    -  $ export REF=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 2 -d ' '); echo REF=$REF
    - "Create the list of files to skip and enshure there are some files in these directories"
    - $ ls -1d /usr/share/locale /usr/share/man /usr/share/zoneinfo > /tmp/skip
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo
    - "Create the commit with changed timestamp and skipped list from above to allow upgrade with recent update file"
    - |
        $ export NEWID=$(sudo ostree commit --orphan --tree=ref=$BOOTID --add-metadata-string=ostree.collection-binding=$CID --bind-ref=$REF --timestamp="1 year ago" --skip-list=/tmp/skip); echo "New commit: $NEWID"
    - "Deploy the prepared commit"
    - $ sudo ostree admin upgrade --allow-downgrade --deploy-only --override-commit=$NEWID --reboot
    - "Wait until the system is booted again and check the deployment"
    - $ sudo ostree admin status
    - "The booted commit (started with '*') must have ID which we prepare and the initial commit ID should be marked as '(rollback)'"
    - "Check booted deployment have no file objects which we skip"
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo
    - "Remove the initial deployment"
    - $ sudo ostree admin undeploy 1
    - "Reboot the system"
    - "Check the current deployment"
    - $ sudo ostree admin status
    - "Start the user interface agent with mode preventing automatic system reboot after update"
    - $ sudo updatectl --register-upgrade-handler &
    - "Plug the USB flash drive into the device"
    - "The update starts automatically"
    - "After the update, the device does *not* reboot automatically"
    - "Check that the user interface agent reports the pending update"
    - |
        >** Message: Upgrade status: Checking
         ** Message: An upgrade is pending
    - "Remove the USB flash drive"
    - "Check if there is pending deployment and reboot the DUT"
    - $ sudo ostree admin status
    - $ sudo reboot
    - "In `U-Boot` console check the status of upgrade"
    - $ printenv bootcount bootlimit
    - |
        >bootcount=1
        bootlimit=3
    - "Restart the device by pressing the restart button before the boot finishes."
    - "Restart the device a second time by pressing the restart button before the boot finishes."
    - "Restart the device a third time by pressing the restart button before the boot finishes."
    - "U-Boot should be able to detect the rollback mode and boot the system in rollback mode"
    - |
        >Warning: Bootlimit (3) exceeded. Using altbootcmd.
        Hit any key to stop autoboot:  0 
        switch to partitions #0, OK
        mmc0 is current device
        Scanning mmc 0:1...
        Found /extlinux/extlinux-rollback.conf
        Retrieving file: /extlinux/extlinux-rollback.conf
    - "Wait for system boot"
    - "Check the update has been rolled back and that the initial deployment is still the active deployment"
    - $ sudo ostree admin status
    - "Start the user interface agent"
    - $ sudo updatectl &
    - "Plug the USB flash drive with the same update file into the device"
    - "Check that the user interface agent reports the system is up to update"
    - |
        >** Message: 09:21:45.026: Upgrade status: Checking
        ** Message: 09:21:45.300: System is up to date
    - "Check the journal log should mention that the update ID has been blacklisted"
    - $ sudo journalctl -ef --unit apertis-update-manager
    - |
        >May 01 09:21:45 apertis apertis-update-[363]: mount added : /media/APERTIS
        May 01 09:21:45 apertis apertis-update-[363]: Ostree static delta starting
        May 01 09:21:45 apertis apertis-update-[363]: Metadata read from commit '710dbec2943510e4deb279cd6738a4a1a5b589eb6c4976e486d056e0308a02e1': {'ostree.ref-binding': <['apertis/v2020pre/armhf-uboot/minimal']>, 'ostree.collection-binding': <'org.apertis.os'>}
        May 01 09:21:45 apertis apertis-update-[363]: Revision '710dbec2943510e4deb279cd6738a4a1a5b589eb6c4976e486d056e0308a02e1' is marked as blacklisted; skipping
        May 01 09:21:45 apertis apertis-update-[363]: Ostree already up to date

